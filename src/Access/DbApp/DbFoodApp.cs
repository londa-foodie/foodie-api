﻿using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Balance;
using Domain.Db.Requests.Models.Foods;
using Domain.Db.Requests.Models.Users;
using Domain.Enums;
using Microsoft.Extensions.Configuration;
using Mysqlx.Crud;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace Access.DbApp
{
    public class DbFoodApp: DbBase, IDbFoodApp
    {
        public DbFoodApp(IConfiguration config) : base(config.GetConnectionString("Db") ?? "") { }

        #region BALANCE
        public DbResult PostBalance(RequestPostBalance request)
        {
            var con = ConnectionGet();
            try
            {                
                con.Open();
                using(var cmd = con.CreateCommand()) 
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = string.Format("INSERT INTO londa_foodie.balances " + 
                                      "(user_id, food_id, balance, date_added, date_modified, status, payment_method) " +
                                      "VALUES ({0}, {1}, {2}, {3}, {4}, {5}, {6});",
                                      request.UserId,
                                      request.FoodId,
                                      request.Balance,
                                      dt,
                                      dt,
                                      request.Status,
                                      request.PaymentMethod
                                      );

                    return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally 
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public DbResult UpdateBalance(RequestUpdateBalance request)
        {
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "UPDATE londa_foodie.balances " + 
                                      $"SET food_id = {request.FoodId}, balance = {request.Balance}, date_modified = {dt}, status = {request.Status}, payment_method={request.PaymentMethod} " + 
                                      $"WHERE id = {request.Id}";
                    return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public List<DbBalancesInfo> GetBalance(int limit, int offset, out int dc)
        {
            var con = ConnectionGet();
            var balances = new List<DbBalancesInfo>();
            try
            {
                con.Open();
                using(var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "SELECT * FROM londa_foodie.balances";
                    dc = int.Parse(cmd.ExecuteScalar().ToString() ?? "");
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "SELECT * FROM londa_foodie.balances " +
                                      $"ORDER BY date_modified DESC LIMIT {limit} OFFSET {offset}";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read()) 
                    {
                        balances.Add(new DbBalancesInfo()
                        {
                            Id = reader.GetInt32("id"),
                            UserId = reader.GetString("user_id"),
                            FoodId = reader.GetInt32("food_id"),
                            Balance = reader.GetInt32("balance"),
                            DateAdded = reader.GetInt32("date_added"),
                            DateModified = reader.GetInt32("date_modified"),
                            Status = reader.GetInt32("status"),
                            PaymentMethod = reader.GetInt32("payment_method")
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return balances;
        }
        #endregion

        #region FOOD
        public DbResult PostFood(RequestPostFood request)
        {
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    cmd.CommandText = "SELECT MAX(id) FROM londa_foodie.foods;";
                    var max = int.Parse(cmd.ExecuteScalar().ToString() ?? "");
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = string.Format("INSERT INTO londa_foodie.foods " +
                                      "(id, name, date_added, date_modified) " +
                                      "VALUES ({0}, '{1}', {2}, {3});",
                                      max + 1,
                                      request.Name,                                      
                                      dt,
                                      dt                                      
                                      );

                    return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public DbResult UpdateFood(RequestUpdateFood request)
        {
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "UPDATE londa_foodie.balances " +
                                      $"SET name = {request.Id}, total_price = {request.TotalPrice}, date_modified = {dt} " +
                                      $"WHERE id = {request.Id}";
                    return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public List<DbFoodsInfo> GetFood(int limit, int offset, out int dc)
        {
            var foods = new List<DbFoodsInfo>();
            dc = 0;
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) FROM londa_foodie.foods";
                    dc = int.Parse(cmd.ExecuteScalar().ToString() ?? "");
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "SELECT * FROM londa_foodie.foods " +
                                      $"ORDER BY date_added DESC LIMIT {limit} OFFSET {offset}";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        foods.Add(new DbFoodsInfo()
                        {
                            Id = reader.GetInt32("id"),
                            Name = reader.GetString("name"),
                            TotalPrice = reader.GetInt32("total_price"),
                            DateAdded = reader.GetInt32("date_added")
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }

            return foods;
        }
        #endregion

        #region USERS
        public DbResult PostUser(RequestPostUser request)
        {
            var dc = 0;
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    var guid = Guid.NewGuid();
                    cmd.CommandText = $"SELECT COUNT(*) FROM londa_foodie.londa_emp WHERE " +
                        $"f_name='{request.Fname}' AND m_name='{request.MName}' AND l_name='{request.LName}' " + 
                        $"OR phone_number='{request.PhoneNumber}' OR email='{request.Email}'";
                    dc = int.Parse(cmd.ExecuteScalar().ToString() ?? "");

                    if (dc == 0) 
                    {                     
                        cmd.CommandTimeout = 120;
                        cmd.CommandText = string.Format("INSERT INTO londa_foodie.londa_emp " +
                                          "(id, f_name, m_name, l_name, email, password, phone_number, date_added, date_modified, status, v_timeout) " +
                                          "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}',{7}, {8}, {9}, {10});",
                                          guid,
                                          request.Fname,
                                          request.MName,
                                          request.LName,
                                          request.Email,
                                          request.Password,
                                          request.PhoneNumber,
                                          dt,
                                          dt,
                                          0,
                                          120000
                                          );

                        return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                    }
                    else
                    {
                        return DbResult.Existed;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public DbResult UpdateUser(RequestUpdateUser request)
        {
            var con = ConnectionGet();
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    var dt = DateTimeOffset.Now.ToUnixTimeSeconds();
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "UPDATE londa_foodie.users " +
                                      $"SET f_name = {request.Fname}, m_name = {request.MName}, l_name = {request.LName}, email = {request.Email}, password={request.Password}, phone_number = {request.PhoneNumber}, date_added = {dt}, date_modified = {dt}, status={request.Status}" +
                                      $"WHERE id = {request.Id}";
                    return cmd.ExecuteNonQuery() > 0 ? DbResult.Success : DbResult.Error;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        public List<DbUsersInfo> GetUser(int limit, int offset, out int dc)
        {
            var users = new List<DbUsersInfo>();
            var con = ConnectionGet();            
            try
            {
                con.Open();
                using (var cmd = con.CreateCommand())
                {
                    cmd.CommandText = "SELECT COUNT(*) FROM londa_foodie.londa_emp WHERE status=1";
                    dc = int.Parse(cmd.ExecuteScalar().ToString() ?? "");                    
                    cmd.CommandTimeout = 120;
                    cmd.CommandText = "SELECT * FROM londa_foodie.londa_emp WHERE status=1 " +
                                      $"ORDER BY date_modified DESC LIMIT {limit} OFFSET {offset};";

                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        users.Add(new DbUsersInfo()
                        {
                            Id = reader.GetString("id"),
                            Fname = reader.GetString("f_name"),
                            MName = reader.GetString("m_name"),
                            LName = reader.GetString("l_name"),
                            Email = reader.GetString("email"),
                            Password = reader.GetString("password"),
                            PhoneNumber = reader.GetString("phone_number"),
                            DateAdded = reader.GetInt32("date_added"),
                            DateModified = reader.GetInt32("date_modified"),
                            Status = reader.GetInt32("status"),
                            VerifyTimeout = reader.GetInt32("v_timeout")
                        });
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (con != null && con.State == System.Data.ConnectionState.Open)
                {
                    con.Close();
                    con.Dispose();
                }
            }
            
            return users;
        }
        #endregion
    }
}
