﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Access.DbApp
{
    public class DbBase
    {
        readonly string _constring;

        public DbBase(string constring)
        {
            _constring = constring;
        }

        protected MySqlConnection ConnectionGet()
        {
            return new MySqlConnection()
            {
                ConnectionString = _constring
            };
        }
    }
}
