﻿using Application.FoodApp;
using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Foods;
using Domain.Dto.Models;
using Domain.Dto.Results.Foods;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FoodsController : Controller
    {
        FoodService _foods;
        public FoodsController(IDbFoodApp foods)
        {
            _foods = new FoodService(foods);
        }

        [HttpPost]
        public ActionResult<ResultPostFood> Post(RequestPostFood request)
        {
            return _foods.Post(request);
        }

        [HttpPut]
        public ActionResult<ResultUpdateFood> Put(RequestUpdateFood request)
        {
            return _foods.Update(request);
        }

        [HttpGet]
        public ActionResult<DbFoodList> Get(int limit = 100, int offset = 0)
        {
            return _foods.Get(limit,offset);            
        }
    }
}
