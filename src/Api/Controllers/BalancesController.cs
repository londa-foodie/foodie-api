﻿using Microsoft.AspNetCore.Mvc;
using Application.FoodApp;
using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Balance;
using Domain.Dto.Models;
using Domain.Dto.Results.Balance;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BalancesController : ControllerBase
    {
        BalanceService _balance;

        public BalancesController(IDbFoodApp balance)
        {
            _balance = new BalanceService(balance);
        }

        [HttpPost]
        public ActionResult<ResultPostBalance> Post(RequestPostBalance request)
        {
            return _balance.Post(request);
        }

        [HttpPut]
        public ActionResult<ResultUpdateBalance> Put(RequestUpdateBalance request)
        {
            return _balance.Update(request);
        }

        [HttpGet]
        public ActionResult<DbBalanceList> Get(int limit = 100, int offset = 0)
        {
            return _balance.Get(limit,offset);
        }
    }
}
