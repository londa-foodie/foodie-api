﻿using Application.FoodApp;
using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Users;
using Domain.Dto.Models;
using Domain.Dto.Results.Users;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsersController : Controller
    {
        UserService _users;

        public UsersController(IDbFoodApp users)
        {
            _users = new UserService(users);
        }

        [HttpPost]
        public ActionResult<ResultPostUser> Post(RequestPostUser request)
        {
            return _users.Post(request);            
        }

        [HttpPut]
        public ActionResult<ResultUpdateUser> Put(RequestUpdateUser request)
        {
            return _users.Update(request);
        }

        [HttpGet]
        public ActionResult<DbUsersList> Get(int limit = 100, int offset = 0)
        {
            return _users.Get(limit,offset);
        }
    }
}
