﻿using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Foods;
using Domain.Dto;
using Domain.Dto.Models;
using Domain.Dto.Results.Balance;
using Domain.Dto.Results.Foods;
using Domain.Enums;
using Mysqlx.Crud;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.FoodApp
{
    public class FoodService
    {
        IDbFoodApp _food;

        public FoodService(IDbFoodApp food)
        {
            _food = food;
        }

        public ResultUpdateFood Update(RequestUpdateFood request)
        {
            try
            {
                var res = _food.UpdateFood(request);
                return new ResultUpdateFood()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }

        public ResultPostFood Post(RequestPostFood request)
        {
            try
            {
                var res = _food.PostFood(request);
                return new ResultPostFood()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }

        public DbFoodList Get(int limit, int offset)
        {
            try
            {
                var res = _food.GetFood(limit, offset, out int dc);
                var list = new List<DtoFoodsInfo>(res.Select(x => x.AsDtoFoodsInfo()).ToList());

                var x = new DbFoodList()
                {
                    Count = dc,
                    Result = list,
                };

                return x;
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }
    }
}
