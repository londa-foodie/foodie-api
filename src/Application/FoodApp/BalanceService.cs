﻿using Domain.Db.Interfaces;
using Domain.Dto.Models;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Balance;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Dto.Results.Balance;
using Domain.Dto;
using Mysqlx;

namespace Application.FoodApp
{
    public class BalanceService
    {
        IDbFoodApp _balance;

        public BalanceService(IDbFoodApp balance)
        {
            _balance = balance;
        }

        public ResultPostBalance Post(RequestPostBalance request)
        {
            try
            {
                var res = _balance.PostBalance(request);
                return new ResultPostBalance()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }

        public ResultUpdateBalance Update(RequestUpdateBalance request)
        {
            try
            {
                var res = _balance.UpdateBalance(request);
                return new ResultUpdateBalance()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DbBalanceList Get(int limit, int offset)
        {
            try
            {
                var res = _balance.GetBalance(limit, offset, out int dc);                
                var list = new List<DtoBalancesInfo>(res.Select(x => x.AsDtoBalancesInfo()).ToList());

                var x = new DbBalanceList()
                {
                    Count = dc,
                    Result = list,
                };

                return x;
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }
    }
}
