﻿using Domain.Db.Interfaces;
using Domain.Db.Models;
using Domain.Db.Requests.Models.Users;
using Domain.Dto;
using Domain.Dto.Models;
using Domain.Dto.Results.Foods;
using Domain.Dto.Results.Users;
using Domain.Enums;
using Mysqlx.Crud;
using MySqlX.XDevAPI.Common;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application.FoodApp
{
    public class UserService
    {
        IDbFoodApp _user;

        public UserService(IDbFoodApp user)
        {
            _user = user;   
        }

        public ResultPostUser Post(RequestPostUser request)
        {
            try
            {
                var res = _user.PostUser(request);
                return new ResultPostUser()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }

        public ResultUpdateUser Update(RequestUpdateUser request)
        {
            try
            {
                var res = _user.UpdateUser(request);
                return new ResultUpdateUser()
                {
                    IsSuccess = res == DbResult.Success,
                    Message = res.ToString(),
                    Code = (int)res
                };
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }

        public DbUsersList Get(int limit, int offset)
        {
            try
            {
                var res = _user.GetUser(limit, offset, out int dc);
                
                var list = new List<DtoUsersInfo>(res.Select(x => x.AsDtoUsersInfo()).ToList());

                var x = new DbUsersList()
                {
                    Count = dc,
                    Result = list,
                };

                return x;
            }
            catch (Exception error)
            {
                throw new Exception(error.Message);
            }
        }
    }
}
