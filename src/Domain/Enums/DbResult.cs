﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Enums
{
    public enum DbResult
    {
        Success = 1,
        Error = 2,
        Warning = 3,
        Existed = 4,
    }
}
