﻿using Domain.Db.Models;
using Domain.Db.Requests.Models.Balance;
using Domain.Db.Requests.Models.Foods;
using Domain.Db.Requests.Models.Users;
using Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Interfaces
{
    public interface IDbFoodApp
    {
        DbResult PostBalance(RequestPostBalance request);
        DbResult UpdateBalance(RequestUpdateBalance request);
        List<DbBalancesInfo> GetBalance(int limit, int offset, out int dc);

        DbResult PostFood(RequestPostFood request);
        DbResult UpdateFood(RequestUpdateFood request);
        List<DbFoodsInfo> GetFood(int limit, int offset, out int dc);

        DbResult PostUser(RequestPostUser request);
        DbResult UpdateUser(RequestUpdateUser request);
        List<DbUsersInfo> GetUser(int limit, int offset, out int dc);        
    }
}
