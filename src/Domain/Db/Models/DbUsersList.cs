﻿using Domain.Dto.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Models
{
    public class DbFoodList
    {
        public int Count { get; set; }
        public List<DtoFoodsInfo> Result { get; set; }
    }
}
