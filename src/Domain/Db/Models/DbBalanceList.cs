﻿using Domain.Dto.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Models
{
    public class DbBalanceList
    {
        public int Count { get; set; }
        public List<DtoBalancesInfo> Result { get; set; }
    }
}
