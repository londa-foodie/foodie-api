﻿using Domain.Dto.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Models
{
    public class DbUsersList
    {
        public int Count { get; set; }
        public List<DtoUsersInfo> Result { get; set; }
    }
}
