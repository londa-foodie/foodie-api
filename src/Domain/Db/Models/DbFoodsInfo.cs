﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Models
{
    public class DbFoodsInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalPrice { get; set; }
        public int DateAdded { get; set; }
        public int DateModified { get; set; }
    }
}
