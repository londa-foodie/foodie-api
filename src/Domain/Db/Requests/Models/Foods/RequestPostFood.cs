﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Requests.Models.Foods
{
    public class RequestPostFood
    {        
        public string Name { get; set; }
        public int TotalPrice { get; set; }
    }
}
