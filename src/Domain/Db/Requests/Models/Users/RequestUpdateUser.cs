﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Requests.Models.Users
{
    public class RequestUpdateUser
    {
        public string Id { get; set; }
        public string Fname { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
        public int DateAdded { get; set; }
        public int DateModified { get; set; }
        public int Status { get; set; }
    }
}
