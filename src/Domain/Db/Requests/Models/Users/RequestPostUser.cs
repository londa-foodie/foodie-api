﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Db.Requests.Models.Users
{
    public class RequestPostUser
    {
        public string Fname { get; set; }
        public string MName { get; set; }
        public string LName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
    }
}
