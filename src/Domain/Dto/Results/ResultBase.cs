﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results
{
    public class ResultBase
    {
        public int Code { get; set; }
        public string ErrorMsg { get; set; }
        public string Message { get; set; }
    }
}
