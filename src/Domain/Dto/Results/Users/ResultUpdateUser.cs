﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results.Users
{
    public class ResultUpdateUser : ResultBase
    {
        public bool IsSuccess { get; set; }
    }
}
