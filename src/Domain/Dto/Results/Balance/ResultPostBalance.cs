﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results.Balance
{
    public class ResultPostBalance : ResultBase
    {
        public bool IsSuccess { get; set; }
    }
}
