﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results.Balance
{
    public class ResultUpdateBalance : ResultBase
    {
        public bool IsSuccess { get; set; }
    }
}
