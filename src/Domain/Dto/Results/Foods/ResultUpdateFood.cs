﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results.Foods
{
    public class ResultUpdateFood : ResultBase
    {
        public bool IsSuccess { get; set; }
    }
}
