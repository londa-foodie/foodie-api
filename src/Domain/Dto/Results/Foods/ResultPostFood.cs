﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Results.Foods
{
    public class ResultPostFood : ResultBase
    {
        public bool IsSuccess { get; set; }
    }
}
