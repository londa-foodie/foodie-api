﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Models
{
    public class DtoBalancesInfo
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public int FoodId { get; set; }
        public int Balance { get; set; }
        public int DateAdded { get; set; }
        public int DateModified { get; set; }
        public int Status { get; set; }
        public int PaymentMethod { get; set; }
    }
}
