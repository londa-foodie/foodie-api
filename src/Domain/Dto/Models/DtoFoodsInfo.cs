﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto.Models
{
    public class DtoFoodsInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TotalPrice { get; set; }
        public int DateAdded { get; set; }
        public int DateModified { get; set; }
    }
}
