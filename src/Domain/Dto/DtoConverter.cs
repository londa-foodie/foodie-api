﻿using Domain.Db.Models;
using Domain.Dto.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Dto
{
    public static class DtoConverter
    {
        public static DtoBalancesInfo AsDtoBalancesInfo(this DbBalancesInfo b)
        {
            return new DtoBalancesInfo
            {
                Id = b.Id,
                UserId = b.UserId,
                FoodId = b.FoodId,
                Balance = b.Balance,
                DateAdded = b.DateAdded,
                DateModified = b.DateModified,
                Status = b.Status,
                PaymentMethod = b.PaymentMethod
            };
        }

        public static DtoFoodsInfo AsDtoFoodsInfo(this DbFoodsInfo f)
        {
            return new DtoFoodsInfo
            {
                Id = f.Id,
                Name = f.Name,
                TotalPrice = f.TotalPrice,
                DateAdded = f.DateAdded,
                DateModified = f.DateModified,
            };
        }

        public static DtoUsersInfo AsDtoUsersInfo(this DbUsersInfo u)
        {
            return new DtoUsersInfo
            {
                Id= u.Id,
                Fname = u.Fname,
                MName = u.MName,
                LName = u.LName,
                Email = u.Email,
                Password = u.Password,
                PhoneNumber = u.PhoneNumber,
                DateAdded= u.DateAdded,
                DateModified= u.DateModified,
                Status= u.Status,
                VerifyTimeout = u.VerifyTimeout,
            };
        }
    }
}
